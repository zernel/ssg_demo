Rails.application.config.middleware.use OmniAuth::Builder do
  provider :github, '742fbb0280a599ee957d',
    'afe3f856b5bc34349b7e4f92a447ae5f5aeef41f'
  provider :weibo, '1408937818', '613b940d9fe14180aa01ce294e1ddf8a'

  provider :identity, :fields => [:nickname, :email]
end

#OmniAuth.config.on_failure do |env|
  #[302, {'Location' => "/auth/#{env['omniauth.error.strategy'].name}/failure?message=#{env['omniauth.error.type']}"}, ["Redirecting..."]]
#end
