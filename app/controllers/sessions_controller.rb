#encoding: UTF-8

class SessionsController < ActionController::Base
  def create
    auth = request.env['omniauth.auth']
    user = User.from_auth(auth)
    session[:user_id] = user.id

    if Identity.all_of(email: user.email).first
      flash[:notice] = "欢迎 #{user.nickname}"
      redirect_to root_path
    else
      flash[:notice] = "您已成功验证，请创建本站账户"
      redirect_to new_identity_path
    end
  end
end
