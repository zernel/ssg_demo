class User
  include Mongoid::Document
  field :nickname, :type => String
  field :email, :type => String

  validates :nickname, :presence => true
  validates :email, :uniqueness => true
  has_many :authentications

  def add_auth(auth)
    Authentication.create!(:provider => auth[:provider],
                           :uid => auth[:uid], user: self)
    return self
  end

  class << self
    def from_auth(auth)
      locate_auth(auth) || locate_email(auth) || create_user_with_auth(auth)
    end

    def locate_auth(auth)
      Authentication.all_of(provider: auth[:provider],
                            uid: auth[:uid]).first.try(:user)
    end

    def locate_email(auth)
      user = where(email: auth[:info][:email] || nil).first
      return unless user
      user.add_auth(auth)
      user
    end

    def create_user_with_auth(auth)
      user = create!(
        :nickname => auth[:info][:nickname],
        :email => auth[:info][:email] || nil)
        Authentication.create!(:provider => auth[:provider],
                               :uid => auth[:uid], :user => user)
        return user
    end
  end
end
