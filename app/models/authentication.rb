class Authentication
  include Mongoid::Document
  field :provider, :type => String
  field :uid, :type => String
  field :user_id, :type => Integer

  validates :provider, :presence => true, :uniqueness => {:scope => :user_id}
  validates :uid, :presence => true, :uniqueness => {:scope => :provider}
  belongs_to :user
end
