class Identity
  include Mongoid::Document
  include OmniAuth::Identity::Models::Mongoid

  field :nickname, :type => String
  field :email, :type => String
  field :password_digest, :type => String
end
